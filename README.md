Les opérations Linux
=================

Les 2 principaux types d'opérations réalisables dans un terminal sont :
----------------------------------

Opérations sur les fichiers (lister, lire, supprimer, déplacer)
Lancement de programmes


Opérations sur les fichiers
-----------------


ls : lister les fichiers dans le dossier courant (https://man7.org/linux/man-pages/man1/ls.1.html)

rm nomdufichier : supprimer (ReMove) un fichier (https://man7.org/linux/man-pages/man1/rm.1.html)

mv fichierdedepart fichierdarrivee : déplacer (MoVe) un fichier (https://linux.die.net/man/1/mv)

cp fichierdedepart fichierdarrivee : copier (CoPy) un fichier (https://linux.die.net/man/1/cp)

cd chemin : changer (Change Directory) de dossier courant (https://linuxcommand.org/lc3_man_pages/cdh.html)

pwd : afficher le chemin du dossier courant (Print Working Directory) (https://linux.die.net/man/1/pwd)

cat fichier : lire contenu du fichier (https://man7.org/linux/man-pages/man1/cat.1.html)

mkdir dossier : créer un dossier (MaKe DIRectory) (https://man7.org/linux/man-pages/man1/mkdir.1.html)

touch monfichier.ext : créer un fichier vide (https://man7.org/linux/man-pages/man1/touch.1.html)


pwd : print working directory 
le tild veut dire nos dossiers perso 
slash


Lancement de programmes
----------------------------------
Depuis un terminal, vous pouvez lancer une commande directement en tapant le nom du programme.
Par exemple, si vous voulez appeler python et qu'il est installé, vous pouvez simplement taper python suivi des éventuels paramètres.

Note sur les permissions
----------------------------------
Contrairement à d'autres sytèmes d'exploitation qui ne seront pas mentionnés ici, les systèmes Linux appliquent des politiques stricts de gestion des droits et permissions.
Un terminal est toujours ouvert en tant qu'un certain utilisateur.
La commande id (https://man7.org/linux/man-pages/man1/id.1.html) permet d'avoir des informations sur l'utilisateur courant.
On obtient le numéro et le nom de l'utilisateur connécté ainsi que ses éventuels groupes.
Il existe un super-utilisateur : root possédant l'id 0.
Ce super-utilisateur possède tous les droits sur le système.
Il est vivement recommandé de ne pas utiliser le compte root au quotidien mais de ne l'utiliser qu'occasionnellement, lorsqu'on a besoin d'exécuter une commande avec des droits élevés.
La commande su nouvelutilisateur permet de changer d'utilisateur actif, à partir du moment où l'on connait son mot de passe.
La commande sudo command permet d'exécuter une unique commande en tant que root. https://xkcd.com/149/


Notes de cours : 
su do : c'est à dire lancer une commande en tant que super utilisateur ou administrateur


Gestionnaire de paquet
----------------------------------
Les distributions Linux contiennent en général un gestionnaire de paquets permettant d'installer (et mettre à jour / désinstaller) des nouvelles applications / commandes facilement
Dans la suite, on va manipuler apt (aussi appelé apt-get) qui est le gestionnaire de paquets de Debian et de ses dérivés (Ubuntu, Linux mint ...)
Pour installer un paquet, il suffit d'appeler la commande install de apt :
apt install nomdupaquet
Il peut être nécessaire de d'abord mettre à jour la liste des paquets disponibles : apt update
Note : l'utilisation d'apt requiert les droits super-utilisateur (root). Vous allez donc généralement préfixer la commande par sudo

Notes: 
-------
Pour exercice "Que fait figlet hello ?"
Gestionnaire de paquets : apt permettant installer les paquets qu'on a pas
faire commande : apt install figlet 
E: Could not open lock file /var/lib/dpkg/lock-frontend - open (13: Permission denied)
E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), are you root?
il faut donc faire la commande en se placant en tant que super utilisateur 
sudo apt install figlet
Pour la question 2
sudo apt install cmatrix 


Intégration continue
=================
![](https://i.imgur.com/Mkjq6jX.png =500x)

Concepts
----------
Vous avez fait des commandes en terminal durant ce cours, l'idée c'est de généraliser ces commandes dans des scripts a executer souvent pour effectuer des vérifications.
Typiquement :
-----------
Nous avions comme objectif de rendre le code le plus portable possible, c'est bien pour vos collègues mais aussi pour des petits automates qui vont pouvoir faire des vérifications au fur et a mesure de l'avancement du projet.
Et par vérifications on entend*:

le "Linting" à la main => Voir que le code respecte bien des règles établies.
le testing

Rappel tests unitaires : Vérifications des briques élémentaires du projet : ex la fonction ajoutMajuscule(mot) renvoie bien 'Conception' si on lui donne 'conception' en entrée.

(pour aller plus loin) Tests fonctionnels : Vérifications que les fonctionnalités au sens plus global fonctionnent : ex Selenium Cucumber




Puis pas forcément pour le python :
------------------------------------

Le build : compilation du code et des dépendances en livrable.
la release: mise a disposition d'un produit téléchargeable (bin/exe/tar.gz/zip)

*(Après récupération des dépendances projet et avec un environnement qui permet de faire tourner le code)

Pratique
Pour cette partie vous aurez donc besoin d'un dépot git.


Commencez par récupérer le dépot : https://gitlab.com/MDZP17/projet-pytest


Puis créer votre dépôt et notifiez le 


 Les tests ont été effectués avec pytest, c'est une application pas trop complexe, donc ça devrait le faire.

Ensuite, après l'avoir récupéré de votre côté, effectuez en ligne de commandes les operations nécessaires pour linter et tester depuis zéro l'application (notamment la partie pip)

Rappel linters d'exemple : autopep8, flake8, pylint
 - Ajouter la dépendance vers votre linter (flake8, autopep8,..) et faites une nouvelle version via git
(add,commit,push)
 

Puis lancez tests et linter


Vous pouvez effectivement constater qu'il y a des erreurs dans les tests et potentiellement dans le lint : corrigez les

Sauvegarder les commandes sur le côté, rappelez vous juste de ce qu'il vous a fallu pour faire vos manipulations
Aide
